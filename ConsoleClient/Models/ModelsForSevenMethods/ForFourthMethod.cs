﻿using ConsoleClient.Models;
using System.Collections.Generic;

namespace ConsoleClient.ModelsForSevenMethods
{
    public class ForFourthMethod
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
}
