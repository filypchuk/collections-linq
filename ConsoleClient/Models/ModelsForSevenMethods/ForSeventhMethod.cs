﻿using ConsoleClient.Models;

namespace ConsoleClient.ModelsForSevenMethods
{
    public class ForSeventhMethod
    {
        public Project Project { get; set; }
        public Task TheLongestTask { get; set; }
        public Task TheShortestTask { get; set; }
        public int CountUsersInTeam { get; set; }

    }
}
