﻿using ConsoleClient.Models;

namespace ConsoleClient.ModelsForSevenMethods
{
    public class ForSixthMethod
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int CountTasksByLastProject { get; set; }
        public int CountStartCancelTasks { get; set; }
        public Task TheLongestByTimeTask { get; set; }
    }
}
