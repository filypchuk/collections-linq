﻿using ConsoleClient.Models;
using System.Collections.Generic;

namespace ConsoleClient.ModelsForSevenMethods
{
    public class ForFifthMethod
    {
        public User User { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
