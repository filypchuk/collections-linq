﻿namespace ConsoleClient.Models
{
    public enum TaskStates
    {
        Created = 0,
        Started,
        Finished,
        Canceled
    }
}
