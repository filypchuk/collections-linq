﻿using System;
using System.Collections.Generic;

namespace ConsoleClient.Models
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int AuthorId { get; set; }
        public User User { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }
        public IEnumerable<Task> Tasks { get; set; }
        public override string ToString()
        {
            return $" Id -- {Id}\n Name -- {Name}\n" +
                $" CreatedAt -- {CreatedAt}\n FinishedAt -- {Deadline}\n" +
                $" AuthorId -- {AuthorId}\n TeamId -- {TeamId}\n Description -- \"{Description}\" \n";
        }
    }
}
