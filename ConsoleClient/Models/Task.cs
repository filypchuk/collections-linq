﻿using System;

namespace ConsoleClient.Models
{
    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskStates State { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public int PerformerId { get; set; }
        public User User { get; set; }
        public override string ToString()
        {
            return $"Id -- {Id}\n Name -- {Name}\n CreatedAt -- {CreatedAt}\n FinishedAt -- {FinishedAt}\n " +
                $"State -- {State}\n ProjectId -- {ProjectId}\n PerformerId -- {PerformerId}\n Description -- {Description}\n ";
        }
    }
}
