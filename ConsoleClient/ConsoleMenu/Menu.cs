﻿using ConsoleClient.Helpers;
using System;

namespace ConsoleClient.ConsoleMenu
{
    class Menu
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        private readonly PrintingMethods printingMethods;
        public Menu()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            printingMethods = new PrintingMethods();
        }
        public void Start()
        {
            DisplayMenu();
            while (true)
            {
                var keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.D1:
                        printingMethods.PrintFirstMethod();
                        break;
                    case ConsoleKey.D2:
                        printingMethods.PrintSecondMethod();
                        break;
                    case ConsoleKey.D3:
                        printingMethods.PrintThirdMethod();
                        break;
                    case ConsoleKey.D4:
                        printingMethods.PrintFourthMethod();
                        break;
                    case ConsoleKey.D5:
                        printingMethods.PrintFifthMethod();
                        break;
                    case ConsoleKey.D6:
                        printingMethods.PrintSixthMethod();
                        break;
                    case ConsoleKey.D7:
                        printingMethods.PrintSeventhMethod();
                        break;
                    default:
                        DisplayMenu();
                        break;
                }
                DisplayMenu();
            }
        }
        private void DisplayMenu()
        {
            Console.Clear();
            print("Click button 1-7 to select method", Color.Red);
            print(" ", Color.None);
            print("1---Get the number of tasks in a project of a particular user (userId)", Color.Green);
            print(" ", Color.None);
            print("2---Get a list of tasks designed for a specific user (userId)", Color.Green);
            print(" ", Color.None);
            print("3---List of completed tasks in 2020 (userId)", Color.Green);
            print(" ", Color.None);
            print("4---Get users of teams", Color.Green);
            print(" ", Color.None);
            print("5---Get a list of users and user's tasks", Color.Green);
            print(" ", Color.None);
            print("6---Get struct six (userId)", Color.Green);
            print(" ", Color.None);
            print("7---Get struct seven - for all projects", Color.Green);
            print(" ", Color.None);
        }
    }
}
