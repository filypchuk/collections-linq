﻿using ConsoleClient.Helpers;
using ConsoleClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleClient.ConsoleMenu
{
    public class PrintingMethods
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        private readonly WaitingForEnter printWaitingForEnter;
        private readonly CheckInput checkInput;
        private readonly SevenMethods _sevenMethods;
        public PrintingMethods()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            printWaitingForEnter = printToConsole.WaitEnter;
            checkInput = new CheckInput();
            _sevenMethods = new SevenMethods();
        }

        public void PrintFirstMethod()
        {
            Console.Clear();
            print("First Method", Color.Yellow);
            print("Get the number of tasks in the project of a particular user (by id)\n" +
                "(dictionary, where the key will be the project, and value the number of tasks).", Color.None);
            print("Enter user id", Color.Green);
            int userId = checkInput.CheckingInt();
                Dictionary<Project, int> dic = _sevenMethods.FirstMethod(userId);
                if (!EmptyList(dic))
                {
                    foreach (KeyValuePair<Project, int> keyValue in dic)
                    {
                        print("Project", Color.Yellow);
                        print(keyValue.Key.ToString(), Color.Green);
                        print("Total tasks in project " + keyValue.Value, Color.Yellow);
                        print("-------------------------------", Color.Blue);
                    }
                }
                else print("User not found or user doesn't have any project or something else", Color.Red);
            printWaitingForEnter();
        }
        public void PrintSecondMethod()
        {
            Console.Clear();
            print("Second Method", Color.Yellow);
            print("Get a list of tasks designed for a specific user (by id),\n" +
                "where name task < 45 characters (collection of tasks).", Color.None);
            print("Enter user id", Color.Green);
            int userId = checkInput.CheckingInt();
                List<Task> tasks = _sevenMethods.SecondMethod(userId);
                if (!EmptyList(tasks))
                {
                    foreach (var task in tasks)
                    {
                        print("Task", Color.None);
                        print(task.ToString(), Color.Green);
                        print("-------------------------------", Color.Blue);
                    }
                }
                else print("User not found or user doesn't have any task or something else", Color.Red);
            printWaitingForEnter();
        }
        public void PrintThirdMethod()
        {
            Console.Clear();
            print("Third Method", Color.Yellow);
            print("Get a list (id, name) from the collection of tasks that are finished \n" +
                "in the current (2020) year for a specific user (by id).", Color.None);
            print("Enter user id", Color.Green);
            int userId = checkInput.CheckingInt();
            var list = _sevenMethods.ThirdMethod(userId);
            if (!EmptyList(list))
            {
                foreach (var (id, name) in list)
                {
                    print("Task", Color.None);
                    print($"Id -- {id} \t Name -- {name}", Color.Green);
                    print("-------------------------------", Color.Blue);
                }
            }
            else print("User not found or user didn't finish any task in 2020 or something else", Color.Red);
            printWaitingForEnter();
        }
        public void PrintFourthMethod()
        {
            Console.Clear();
            print("Fourth Method", Color.Yellow);
            print("Get a list (id, team name and user list) from a collection of teams over 10 years old, \n" +
                "sorted by date of user registration in descending order, and grouped by team.", Color.None);
            var list = _sevenMethods.FourthMethod();
            if (!EmptyList(list))
            {
                foreach (var i in list)
                {
                    print($"Id -- {i.Id} \t Name -- {i.Name}", Color.Green);
                    if (!EmptyList(list))
                    {
                        print("Users", Color.Yellow);
                        foreach (var user in i.Users)
                        {
                            print(user.ToString(), Color.Yellow);
                        }      
                    }
                    print("-------------------------------", Color.Blue);
                }
            }
            printWaitingForEnter();
        }
        public void PrintFifthMethod()
        {
            Console.Clear();
            print("Fifth Method", Color.Yellow);
            print("Get a list of users in alphabetical order first_name (ascending) " +
                "with sorted tasks by length name (descending).", Color.None);
            var list = _sevenMethods.FifthMethod();
            if (!EmptyList(list))
            {
                foreach (var i in list)
                {
                    print("User", Color.None);
                    print(i.User.ToString(), Color.Green);

                    print("Tasks", Color.Yellow);
                    if (!EmptyList(list))
                    {
                        foreach (var task in i.Tasks)
                        {
                            print(task.ToString(), Color.Yellow);
                        }
                    }
                    else print("User doesn't have tasks or something else", Color.Red);
                    print("-------------------------------", Color.Blue);
                }
            }
            else print("Users not found or something else", Color.Red);
            printWaitingForEnter();
        }
        public void PrintSixthMethod()
        {
            Console.Clear();
            print("Sixth Method", Color.Yellow);
            print("Enter user id", Color.Green);
            int userId = checkInput.CheckingInt();
            var result = _sevenMethods.SixthMethod(userId);

            if (!NoContent(result?.User))
            {
                print("User", Color.Yellow);
                print(result.User.ToString(), Color.Green);       
                if (!NoContent(result.LastProject))
                {
                    print("Last user project (by creation date)", Color.Yellow);
                    print(result.LastProject.ToString(), Color.Green);
                }
                else print("User doesn't have any projcet or something else", Color.Red);
                print("The total number of tasks under the last project", Color.Yellow);
                print(result.CountTasksByLastProject.ToString(), Color.Green);                
                print("The total number of started or canceled tasks for the user", Color.Yellow);
                print(result.CountStartCancelTasks.ToString(), Color.Green);
                    
                if (!NoContent(result.TheLongestByTimeTask))
                {
                    print("The user's longest task by date (earliest created - latest completed)", Color.Yellow);
                    print(result.TheLongestByTimeTask.ToString(), Color.Green);
                }
                else print("User doesn't have any tasks or something else", Color.Red);
                }
            else print("User not found or something else", Color.Red);
            print("-------------------------------", Color.Blue);
            printWaitingForEnter();
        }
        public void PrintSeventhMethod()
        {
            Console.Clear();
            print("Seventh Method", Color.Yellow);
            var list = _sevenMethods.SeventhMethod();
            foreach (var i in list)
            {
                print("Project", Color.Yellow);
                print(i.Project.ToString(), Color.Green);
                print("The longest task of the project (by description)", Color.Yellow);
                if (!NoContent(i.TheLongestTask))
                {
                    print(i.TheLongestTask.ToString(), Color.Green);
                }
                print("The shortest task of the project (by name)", Color.Yellow);
                if (!NoContent(i.TheShortestTask))
                {
                    print(i.TheShortestTask.ToString(), Color.Green);
                }              
                print("The total number of users in the project team, \n " +
                    "where either the project description > 20 characters, or the count of tasks < 3", Color.Yellow);
                print(i.CountUsersInTeam.ToString(), Color.Green);
                print("-------------------------------", Color.Blue);
            }
            printWaitingForEnter();
        }
        private bool NoContent(Object ob)
        {
            if (ob == null)
            {
                print("No Content", Color.Red);
                return true;
            }
            else
                return false;
        }
        private bool EmptyList<T>(IEnumerable<T> ob)
        {
            if (ob.Count() == 0)
            {
                print("Empty List", Color.Red);
                return true;
            }
            else
                return false;
        }
    }
}
