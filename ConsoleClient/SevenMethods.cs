﻿using ConsoleClient.Models;
using ConsoleClient.ModelsForSevenMethods;
using ConsoleClient.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleClient
{
    public class SevenMethods
    {
        private readonly Repository _repository;
        public SevenMethods()
        {
            _repository = new Repository();
        }
        public Dictionary<Project, int> FirstMethod(int userId)
        {
            return _repository.Projects
                .Where(p => p.AuthorId == userId)
                .Select((project) => 
                    new 
                    { 
                        key = project, 
                        value = project.Tasks.Count() 
                    })
                .ToDictionary(x => x.key, x => x.value);
        }
        public List<Task> SecondMethod(int userId)
        {
            return _repository.Tasks
                .Where(task => task.PerformerId == userId && task.Name.Length < 45)
                .ToList();
        }
        public List<(int id, string name)> ThirdMethod(int userId)
        {
            return _repository.Tasks
                .Where(task => task.PerformerId == userId && task.FinishedAt.Year == 2020)
                .Select(t => (t.Id, t.Name))
                .ToList();
        }
        public List<ForFourthMethod> FourthMethod()
        {
            return _repository.Teams
                .Select(team => 
                        new ForFourthMethod
                        {
                            Id = team.Id,
                            Name = team.Name,
                            Users = team.Users
                                    .Where(u => u.Birthday.Year <= 2010)
                                    .OrderByDescending(u => u.RegisteredAt)
                                    .ToList()
                        })
                .Where(team => team.Users.Where(u => u.Birthday.Year <= 2010).Count() != 0 )
                .ToList();
        }
        public List<ForFifthMethod> FifthMethod()
        {
            var users = _repository.Users;

            return users.Select(user => 
                new ForFifthMethod
                {
                    User = user,
                    Tasks = user.Tasks
                        .OrderByDescending(t => t.Name.Length)
                        .ToList()
                })
                .OrderBy(u => u.User.FirstName)
                .ToList();
        }
        public ForSixthMethod SixthMethod(int userId)
        {
            return _repository.Users
                .Where(x => x.Id == userId)
                .Select(user => 
                    new ForSixthMethod
                    {
                        User = user,
                        LastProject = user.Projects.OrderBy(p => p.CreatedAt).LastOrDefault(),
                        CountTasksByLastProject =  user.Projects.OrderBy(p => p.CreatedAt).LastOrDefault() == null ? 0 :
                                 user.Projects.OrderBy(p => p.CreatedAt).LastOrDefault().Tasks.Count(),
                        CountStartCancelTasks =  user.Tasks
                                .Where(t => t.State == TaskStates.Canceled || t.State == TaskStates.Started).Count(),
                        TheLongestByTimeTask = user.Tasks.Where(t => t.PerformerId == userId)
                                .OrderBy(x => x.FinishedAt - x.CreatedAt).LastOrDefault(),
                    })
                .FirstOrDefault();
        }
        public List<ForSeventhMethod> SeventhMethod()
        {
            // Я міг зробити зв'язок project.Team.Users і тоді не використовувати _repository.Users
            // але мені варіант через GroupJoin більше сподобався

            return _repository.Projects
                .GroupJoin(_repository.Users,
                    p => p.TeamId,
                    u => u.TeamId,
                    (project, teamUsers) => new ForSeventhMethod
                        {
                            Project = project,
                            TheLongestTask = project.Tasks.OrderBy(task => task.Description.Length).LastOrDefault(),
                            TheShortestTask = project.Tasks.OrderBy(task => task.Name.Length).FirstOrDefault(),
                            CountUsersInTeam = teamUsers
                                    .Where(u=> project.Description.Length > 20 || project.Tasks.Count() < 3)
                                    .Count()
                        })
                .ToList();
        }
    }
}
