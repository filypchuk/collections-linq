﻿using ConsoleClient.ConsoleMenu;
using System;

namespace ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Loading...");
            Menu menu = new Menu();
            menu.Start();

        }
    }
}
