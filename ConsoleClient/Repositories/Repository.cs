﻿using ConsoleClient.Models;
using System.Collections.Generic;
using ConsoleClient.HttpRequests;
using System.Linq;
using System;

namespace ConsoleClient.Repositories
{
    public class Repository
    {
        private readonly HttpRequest _request;
        public IEnumerable<Project> Projects { get; private set; }
        public IEnumerable<Task> Tasks { get; private set; }
        public IEnumerable<Team> Teams { get; set; }
        public IEnumerable<User> Users { get; private set; }
        public Repository()
        {
            _request = new HttpRequest();
            Projects = _request.GetAll<Project>("projects");
            Tasks = _request.GetAll<Task>("tasks");
            Teams = _request.GetAll<Team>("teams");
            Users = _request.GetAll<User>("users");
            JoinAllEntity();

        }
        private void JoinAllEntity()
        {
            Projects = from project in Projects
                       join user in Users on project.AuthorId equals user.Id
                       join team in Teams on project.TeamId equals team.Id
                       join task in Tasks on project.Id equals task.ProjectId into tasks
                       select new Project
                       {
                           Id = project.Id,
                           Name = project.Name,
                           Description = project.Description,
                           CreatedAt = project.CreatedAt,
                           Deadline = project.Deadline,
                           AuthorId = project.AuthorId,
                           User = user,
                           TeamId = project.TeamId,
                           Team = team,
                           Tasks = from t in tasks
                                   join us in Users on t.PerformerId equals us.Id
                                   select new Task
                                   {
                                       Id = t.Id,
                                       Name = t.Name,
                                       Description = t.Description,
                                       CreatedAt = t.CreatedAt,
                                       FinishedAt = t.FinishedAt,
                                       State = t.State,
                                       ProjectId = t.ProjectId,
                                       Project = project,
                                       PerformerId = t.PerformerId,
                                       User = us,
                                   }
                       };

            Users = from user in Users
                    join team in Teams on user.TeamId equals team.Id into teamOrNull
                    join project in Projects on user.Id equals project.AuthorId into projects
                    join task in Tasks on user.Id equals task.PerformerId into tasks
                    from tOrN in teamOrNull.DefaultIfEmpty()
                    select new User
                    { 
                        Id = user.Id,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Email = user.Email,
                        Birthday = user.Birthday,
                        RegisteredAt = user.RegisteredAt,
                        TeamId = user.TeamId,
                        Team = tOrN,
                        Tasks = tasks,
                        Projects = from pr in projects
                                   join t in Tasks on pr.Id equals t.ProjectId into tasksToProject
                                   select new Project
                                   {
                                       Id = pr.Id,
                                       Name = pr.Name,
                                       Description = pr.Description,
                                       CreatedAt = pr.CreatedAt,
                                       Deadline = pr.Deadline,
                                       AuthorId = pr.AuthorId,
                                       User = user,
                                       TeamId = pr.TeamId,
                                       Team = pr.Team,
                                       Tasks = tasksToProject
                                   }
                    };

            Teams = from team in Teams
                    join user in Users on team.Id equals user.TeamId into users
                    select new Team
                    {
                        Id = team.Id,
                        Name = team.Name,
                        CreatedAt = team.CreatedAt,
                        Users = users
                    };

            Tasks = from task in Tasks
                    join user in Users on task.PerformerId equals user.Id
                    join project in Projects on task.ProjectId equals project.Id
                    select new Task
                    {
                        Id = task.Id,
                        Name = task.Name,
                        Description = task.Description,
                        CreatedAt = task.CreatedAt,
                        FinishedAt = task.FinishedAt,
                        State = task.State,
                        ProjectId = task.ProjectId,
                        Project = project,
                        PerformerId = task.PerformerId,
                        User = user,
                    };

        }
    }
}
