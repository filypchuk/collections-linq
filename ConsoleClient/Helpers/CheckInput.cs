﻿using System;

namespace ConsoleClient.Helpers
{
    class CheckInput
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        public CheckInput()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
        }
        public int CheckingInt()
        {
            bool flag;
            int number;
            do
            {
                string value = Console.ReadLine();
                flag = Int32.TryParse(value, out number);  // If the entered data is incorrect, you must enter it again
                if (!flag)
                {
                    print("Value isn't correct (it must be integer value)", Color.Red);
                }
                else if (number <= 0)                      // Additional check for a positive number
                {
                    flag = false;
                    print("Value must be positive and bigger than 0 ", Color.Red);
                }
            }
            while (!flag);
            return number;
        }
    }
    }
