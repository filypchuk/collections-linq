﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace ConsoleClient.HttpRequests
{
    public class HttpRequest
    {
        private const string URL = "https://bsa20.azurewebsites.net/api";
        private readonly HttpClient _client;
        public HttpRequest()
        {
            _client = new HttpClient();
        }
        public List<T> GetAll<T>(string partUrl)
        {
            var response = _client.GetAsync($"{URL}/{partUrl}");
            string json = response.Result.Content.ReadAsStringAsync().Result;
            var list = JsonConvert.DeserializeObject<List<T>>(json);
            return list;
        }
    }
}
